local sensorInfo = {
	name = "GetTargets"
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current units positions
return function()
	local units = Spring.GetTeamUnits(0)
	local targets = {}
	for i = 1,#units do
		local unitDefID = Spring.GetUnitDefID(units[i])
		local unitDef = UnitDefs[unitDefID]
		if unitDef ~= nil and unitDef.cantBeTransported == false then
			targets[#targets + 1] = units[i]
		end
		
		--if unitDef ~= nil and unitDef.humanName == "Bulldog" then
		--	targets[#targets + 1] = units[i]
		--end
	end
	return targets
end