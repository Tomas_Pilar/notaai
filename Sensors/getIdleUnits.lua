local sensorInfo = {
	name = "getIdleUnits",
	desc = "Return true/false whether the position is empty for unload",
	author = "tomasp",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetUnitPosition = Spring.GetUnitPosition

-- @description return current units positions
return function(units)
	local idleUnits = {}
	for i = 1, #units do
		if not Spring.GetUnitIsActive(units[i]) then
			idleUnits[#idleUnits] = units[i]
		end
	end
	return idleUnits
end