local sensorInfo = {
	name = "RemoveTarget"
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current units positions
return function(targets, toRemove)
	local res = {}
	for i = 1,#targets do
		if targets[i] ~= toRemove then
			res[#res + 1] = targets[i]
		end
	end
	return res
end