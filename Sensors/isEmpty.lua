local sensorInfo = {
	name = "isEmpty",
	desc = "Return true/false whether the position is empty for unload",
	author = "tomasp",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetUnitPosition = Spring.GetUnitPosition

-- @description return current units positions
return function(position, transporterID, transporteeID)
	local units = Spring.GetUnitsInRectangle(position.x - 50, position.z - 50, position.x + 50, position.z + 50)
	for i = 1, #units do
		if units[i] ~= transporteeID and units[i] ~= transporterID then
			return false
		end
	end
	return true
end