function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load the specified unit",
		parameterDefs = {
			{ 
				name = "transporteeID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unitID",
			},
			{ 
				name = "transporterID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unitID",
			},
		}
	}
end

local function ClearState(self)
	self.init = false
end

function Run(self, units, parameter)	
	if not self.init then
		Spring.GiveOrderToUnit(parameter.transporterID, CMD.LOAD_UNITS, {parameter.transporteeID}, {})
		self.init = true
	end
	
	if (parameter.transporterID == Spring.GetUnitTransporter(parameter.transporteeID)) then
		return SUCCESS
	end
	
	return RUNNING
end

function Reset(self)
	ClearState(self)
end