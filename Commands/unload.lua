function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "count",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	local pointman = units[1] -- while this is running, we know that #units > 0, so pointman is valid
	local pointX, pointY, pointZ = SpringGetUnitPosition(pointman)
	local args = {pointX,pointY,pointZ,100}

	if self.loaded == nil then
		self.loaded = 0
	end
	
	if self.loaded < parameter.count then
		SpringGiveOrderToUnit(pointman, CMD.UNLOAD_UNITS, args, {})
		self.loaded = self.loaded + 1	
		return RUNNING
	else
		return SUCCESS
	end
end

function Reset(self)
	self.loaded = 0
end